INTRODUCTION
------------

This module is the extension of the Rendered entity formatters for the field
types entity_reference_revision.

It provides the ability to predefined view modes per each paragraph bundle
separately in the paragraph reference field.

* For a full description of this module, visit the project page:
https://www.drupal.org/project/view_modes_formatter_settings

* To submit bug reports and feature suggestions, or track changes:
https://www.drupal.org/project/issues/view_modes_formatter_settings

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

To use the settings of this module navigate to "Structure > Content Types" and
select any existing content type or can add a new content type and create
entity_reference_revision field type and choose paragraph bundles available for
this field. Then click on Manage Display and select format Rendered Entity for
created field and click on cogwheel which is on the right side and put settings
as needed.

MAINTAINERS
-----------

Current maintainers:
 * Alexander Levitsky - https://www.drupal.org/u/alexanderlevitsky

This project has been sponsored by:
* EPAM Systems
  Since 1993, EPAM Systems, Inc. (NYSE:EPAM) has leveraged its software
  engineering expertise to become a leading global product development, digital
  platform engineering, and top digital and product design agency. Through its
  ‘Engineering DNA’ and innovative strategy, consulting, and design
  capabilities, EPAM works in collaboration with its customers to deliver
  next-gen solutions that turn complex business challenges into real business
  outcomes. EPAM’s global teams serve customers in more than 30 countries across
  North America, Europe, Asia and Australia. As a recognized market leader in
  multiple categories among top global independent research agencies, EPAM was
  one of only four technology companies to appear on Forbes 25 Fastest Growing
  Public Tech Companies list every year of publication since 2013 and was the
  only IT services company featured on Fortune’s 100 Fastest-Growing Companies
  list of 2019.

Learn more at www.epam.com
